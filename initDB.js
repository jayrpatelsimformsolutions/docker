const mongoose = require('mongoose');

module.exports = () => {
  mongoose.connect('mongodb://127.0.0.1:27017/task-manager', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(() => {
    console.log('Mongodb connected....');
  })
    .catch(err => console.log(err.message));
}
